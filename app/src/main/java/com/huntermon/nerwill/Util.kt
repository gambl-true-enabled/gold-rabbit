package com.huntermon.nerwill

import android.content.Context
import android.webkit.JavascriptInterface


fun Float.convertPx2Dp(context: Context): Float {
    return this / context.resources.displayMetrics.density
}

private const val RABBIT_TABLE = "com.RABBIT.table.123"
private const val RABBIT_ARGS = "com.RABBIT.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(RABBIT_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(RABBIT_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(RABBIT_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(RABBIT_ARGS, null)
}
