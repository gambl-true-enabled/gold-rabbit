package com.huntermon.nerwill

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.game_fragment.*
import kotlin.random.Random


class GameFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.game_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        game_container.addView(DrawView(context))
    }

    internal class DrawView(context: Context?) : SurfaceView(context),

        SurfaceHolder.Callback {

        private var newSet = mutableListOf(false, false, false, false, false, false)
        private var isStart = false
        private var count = 0
        private var drawThread: DrawThread? = null
        val automat = BitmapFactory.decodeResource(resources, R.drawable.automat)
        val background = BitmapFactory.decodeResource(resources, R.drawable.background)
        val label = BitmapFactory.decodeResource(resources, R.drawable.label_app)
        val smallRabbit = BitmapFactory.decodeResource(resources, R.drawable.small_rabbit)
        val mediumRabbit = BitmapFactory.decodeResource(resources, R.drawable.medium_rabbit)
        private var widthS = 0
        private var heightS = 0


        override fun surfaceChanged(
            holder: SurfaceHolder, format: Int, width: Int,
            height: Int
        ) {
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            val metrics = context!!.resources.displayMetrics
            widthS = metrics.widthPixels
            heightS = metrics.heightPixels
            drawThread = DrawThread(getHolder())
            drawThread!!.setRunning(true)
            drawThread!!.start()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            var retry = true
            drawThread!!.setRunning(false)
            while (retry) {
                try {
                    drawThread!!.join()
                    retry = false
                } catch (e: InterruptedException) {
                }
            }
        }

        override fun onTouchEvent(event: MotionEvent?): Boolean {
            if (event!!.action == MotionEvent.ACTION_DOWN) {

            } else {
                if (event.action == MotionEvent.ACTION_UP) {
                    if (!isStart && event.x > widthS * 0.14 && event.x < widthS * 0.879 && event.y > heightS * 0.737 && event.y < heightS * 1) {
                        isStart = true
                        startHandler()
                    } else {
                        if (event.x > widthS * 0.287 && event.x < widthS * 0.453 && event.y > heightS * 0.55 && event.y < heightS * 0.609) {
                            click1()
                        } else {
                            if (event.x > widthS * 0.453 && event.x < widthS * 0.62 && event.y > heightS * 0.55 && event.y < heightS * 0.609) {
                                click2()
                            } else {
                                if (event.x > widthS * 0.62 && event.x < widthS * 0.787 && event.y > heightS * 0.55 && event.y < heightS * 0.609) {
                                    click3()
                                } else {
                                    if (event.x > widthS * 0.25 && event.x < widthS * 0.449 && event.y > heightS * 0.609 && event.y < heightS * 0.688) {
                                        click4()
                                    } else {
                                        if (event.x > widthS * 0.449 && event.x < widthS * 0.643 && event.y > heightS * 0.609 && event.y < heightS * 0.688) {
                                            click5()
                                        } else {
                                            if (event.x > widthS * 0.643 && event.x < widthS * 0.833 && event.y > heightS * 0.609 && event.y < heightS * 0.688) {
                                                click6()
                                            } else {

                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {

                }
            }
            return true

        }

        private fun startHandler() {
            val mStatusChecker: Runnable = object : Runnable {
                override fun run() {
                    try {
                        newSet = (1..6).map { Random.nextBoolean() }.toMutableList()
                    } finally {
                        Handler().postDelayed(this, 1000)
                    }
                }
            }
            mStatusChecker.run()
        }

        fun click1() {
            if (newSet[0]) {
                newSet[0] = false
                count++
            }
        }

        fun click2() {
            if (newSet[1]) {
                newSet[1] = false
                count++
            }
        }

        fun click3() {
            if (newSet[2]) {
                newSet[2] = false
                count++
            }
        }

        fun click4() {
            if (newSet[3]) {
                newSet[3] = false
                count++
            }
        }

        fun click5() {
            if (newSet[4]) {
                newSet[4] = false
                count++
            }
        }

        fun click6() {
            if (newSet[5]) {
                newSet[5] = false
                count++
            }
        }

        internal inner class DrawThread(private val surfaceHolder: SurfaceHolder) :
            Thread() {
            private var running = false
            fun setRunning(running: Boolean) {
                this.running = running
            }


            override fun run() {
                var canvas: Canvas?
                while (running) {
                    canvas = null
                    try {
                        canvas = surfaceHolder.lockCanvas(null)
                        if (canvas == null) continue
                        canvas.drawBitmap(background, widthS * -0.722f, heightS * -0.147f, Paint())
                        canvas.drawBitmap(label, widthS * 0.101f, heightS * 0.063f, Paint())
                        canvas.drawBitmap(automat, widthS * 0.138f, heightS * 0.393f, Paint())
                        if (newSet[0])
                            canvas.drawBitmap(
                                smallRabbit,
                                widthS * 0.287f,
                                heightS * 0.55f,
                                Paint()
                            )
                        if (newSet[1])
                            canvas.drawBitmap(
                                smallRabbit,
                                widthS * 0.453f,
                                heightS * 0.55f,
                                Paint()
                            )
                        if (newSet[2])
                            canvas.drawBitmap(smallRabbit, widthS * 0.62f, heightS * 0.55f, Paint())

                        if (newSet[3])
                            canvas.drawBitmap(
                                mediumRabbit,
                                widthS * 0.25f,
                                heightS * 0.609f,
                                Paint()
                            )
                        if (newSet[4])
                            canvas.drawBitmap(
                                mediumRabbit,
                                widthS * 0.449f,
                                heightS * 0.609f,
                                Paint()
                            )
                        if (newSet[5])
                            canvas.drawBitmap(
                                mediumRabbit,
                                widthS * 0.643f,
                                heightS * 0.609f,
                                Paint()
                            )

                        val paint = Paint()
                        if (!isStart) {
                            paint.color = Color.BLACK
                            paint.textSize = heightS * 0.0294f
                            val typeface1 =
                                ResourcesCompat.getFont(context, R.font.montserrat_bold);
                            paint.typeface = typeface1
                            canvas.drawText("START", widthS * 0.407f, heightS * 0.845f, paint)
                        } else {
                            paint.color = Color.parseColor("#ECDB71")
                            paint.textSize = heightS * 0.088f
                            val typeface =
                                ResourcesCompat.getFont(context, R.font.bebas_neue_regular);
                            paint.typeface = typeface

                            canvas.drawText(
                                count.toString(),
                                (widthS * 0.463f - (count.toString().length - 1) * 0.037f * widthS).toFloat(),
                                heightS * 0.53f,
                                paint
                            )
                        }

                    } finally {
                        if (canvas != null) {
                            surfaceHolder.unlockCanvasAndPost(canvas)
                        }
                    }
                }
            }

        }

        init {
            holder.addCallback(this)
        }
    }
}